# Extras

## Multipass Cloud-Config

You can pass the cloud-config.yaml file to `multipass launch` to get a VM that has had most of the setup done for you. This hasn't been tested in cloud-init contexts other than [Multipass](https://multipass.run) so if you use this in another context it may not work for you. This file is "user-data" only. It doesn't handle any disk allocations, networking, or other VM setup. It also assumes that an Ubuntu Image, or at minimum, an image that uses `apt` as its package manager, is being used as the base image.

```sh
# Running this script with Ubuntu 20.04 as the base image results in 2.8GB disk utilization
multipass launch --mem 2G --disk 5G lts --cloud-init /path/to/cloud-config.yaml
```

This config includes options for passing in a database fixture (backed up from another Mobiliaire instance) and pre-existing static files. 

This config requires editing before using:
- Uncomment optional packages if you want/need them.
- Paste a randomized, generated Django secret key in where indicated.
- Uncomment optional Samba share mounting if you want/need to copy files in.
- Select whether you want to install the base pip requirements or the dev pip requirements (defaults to "base")
- Uncomment optional database fixture loading command if you need to load any

Also note that the very end of the file walks you through the rest of the process of:
- editing the settings file to add the IP address or hostname to ALLOWED_HOSTS
- activating the virtual environment
- creating a superuser if needed
- launching the dev server
- viewing the site in a browser
