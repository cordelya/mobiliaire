# Mobiliaire
A lightweight property inventory helper
:
> mobiliaire (n.): Middle French form of "mobiliary"; pertaining to furniture or movable property

*built for/on Django 3.1*

To date, Cordelya has put [![wakatime](https://wakatime.com/badge/user/5469feb2-a822-4cc7-96ff-c4a24388aee1/project/0a2d03f0-f3be-45ed-8adf-ed7c137489fd.svg?style=fflat-square)](https://wakatime.com/badge/user/5469feb2-a822-4cc7-96ff-c4a24388aee1/project/0a2d03f0-f3be-45ed-8adf-ed7c137489fd) into this project.

If you found this app helpful, you can [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/L4L26CZXR)

[Skip to the Changelog](https://gitlab.com/Cordelya/mobiliaire#Changelog)

This self-hosted app is for assisting SCA branches in managing both annual inventories and pack-in/pack-out at events. It is designed to be installed on a portable machine - to be taken to events and serve up web pages with inventory information without needing access to the Internet. A [$35 Raspberry Pi](https://www.raspberrypi.org/products/) fits the bill nicely here, but take my advice and spend the extra $10 for a [case](https://www.raspberrypi.org/products/raspberry-pi-4-case/) if you go that route. An old reimaged notebook would also work for this. Pair it with a spare wifi router and your staff can access the app from their own devices wherever the wifi reaches.

The camera support originially built-in for Raspberry Pi devices (using the official Raspberry Pi camera module) with [picamera](https://github.com/waveform80/picamera) has been rolled into this repository and USB webcam support has been added. Administrators should set the appropriate constant value for CAMERA, at the very bottom of `inventory/settings.py`. Camera support is turned off by default.

** Camera Note:** If you run this app in a production environment (ie using a real webserver and DB as opposed to the three-textfiles-in-a-trenchcoat Django dev server) you will lose access to /dev/video0 (USB webcam source) unless you make some additional permissions changes in the OS. Other parts of this app are currently the focus, so the secret sauce to gaining camera access in a production environment isn't being worked out right now. See [Issue #60](https://gitlab.com/cordelya/mobiliaire/-/issues/60)
An alternative for production servers (with SSL) using client-side image capture is slated for future development. See [Issue #62](https://gitlab.com/cordelya/mobiliaire/-/issues/62)

**New in this version:** optional edit buttons for warehouses, boxes, and items on gallery pages.
* can be turned on/off by modifying the `EDIT_BUTTONS` constant at the bottom of `inventory/settings.py`
* when `EDIT_BUTTONS` is set to `True`, an "Edit" button will appear just below the "View" button on the gallery card. An "Edit" link will also appear in the heading block of each Warehouse, Box, or Item detail page for that Warehouse, Box, or Item.
* The change form is loaded in a _blank target (for most, that means a new tab) 
* if a user is not logged in, the app will redirect to the login page, and after a successful login will forward the user to the target change form

See it in action at https://cordelya.pythonanywhere.com (note: this will be whatever's in the main branch - changes in dev branches won't be available) minus the webcam support because that won't work there!

Want to try it out? 

````
$ git clone https://gitlab.com/cordelya/mobiliaire.git
````
[See a more detailed procedure.](https://gitlab.com/cordelya/mobiliaire/-/wikis/getStarted)

Inventory Items are grouped into Boxes (which can be literal or virtual boxes), which are then further grouped into Warehouses. In this context, a warehouse is a geographic location where a box or boxes are stored. A branch cargo trailer full of items is a Warehouse. Examples of virtual boxes include the "loose in the trailer" box, the "driver-side cargo rack" box, or the "gold key closet" box. 

Key: (N) = not yet implemented

### Put Data in, Get Data Out ###
* Tracks the replacement value of each item. Front-end displays total aggregate value on index, and sub-values on Warehouse and Box detail pages. Why? Insurance.
    * Value data broken down by consumable/not consumable and other breakpoints is planned for a future release.
* PDF/Excel/CSV/Print export of any data on reports pages. Each table has its own set of export buttons, allowing you to build a custom report. You can filter tables before exporting and only export the filtered rows. You can sort columns before exporting and the sort order will be reflected in the result.
   * Page /reports/full/ has all data presented, requiring the report-generator to click only four buttons (warehouse list, boxes by ID, boxes by name, and items). 
   * At this time, print or downlodable pdf reports with each box separated can be done via the reports/wh/ page but it requires clicking each box's export button. One-click button triggering is deferred until after version 1.0 is released.
* ~~CSV import is implemented but it is up to the site admin to set up the import config for each csv file to be imported. This means you can, in theory, config the importer to import your entire inventory spreadsheet, no matter what condition it is in. For the rest of us, start with staff, warehouses, and keywords, then do boxes, then items and attach your items to the appropriate box and keywords.~~ ***Feature not available in v2.0 as the `csvimport` library doesn't support Django 4.***
* Export database as database-friendly backup file (excluding the auth.permission and contenttypes tables will reduce errors on db load later. If you exclude the auth.permission table you will need to create a new superuser when you load this exported data into a fresh install. If you're loading the data into a read-only instance, you don't need to create a superuser - nobody will be able to log in to the admin side.)
~~~~bash
$ ./manage.py dumpdata --exclude auth.permission --exclude contenttypes > db.json
~~~~
* A dashboard showing various information about the state of your inventory is planned for a future release.

### Faster Pack-In/Out and Inventories ###
* Set keywords for each item to make search results filter-able. Add as many keywords as applicable. Suggested keywords include "Category: <category>" for categories like "kitchen" "archery" "heraldic" etc; "Color: <color>" to classify items by their main color(s); "Material: <material>" to classify by material (glass, metal, plastic, etc)
    * Keywords show on item detail page
    * Keyword detail pages list all items associated with a particular keyword
* Pack-in/out friendly search: when an item is needed or arrives for packing, begin search with general item description (ie "ladle"), filter items by keyword (Category: Kitchen, Material: Metal, etc.) until matching item is identified (verify visually via inline photo) and place item in indicated box. Can go very quickly if a person familiar with the system is staffing the search terminal. For best results, limit search query to a single word (complex search planned for after version 1.0 is released)
* Items report table (/reports/items/) is also fully searchable, but does not display pictures and does not include keywords. A user would need to click through to each search result to verify. Displaying item image files via Bootstrap modal is planned for after 1.0 is released.
* Search bar allows simple searching (limit your query to a single word) across Warehouses, Boxes, Items, and Keywords, showing results from each.
* Did you rearrange? Move items from one box to another by setting the current box's end date to today and creating a new items_in_boxes record for the new box, leaving the end date blank. Item's box history will be preserved. Front end shows only current location. 
    * When someone says, "Doesn't this item belong in {former box}?" admin can check the item's box history and see that it used to be in box A but now is assigned to box B.
    * If you know when an item was acquired, you can back-date the item's oldest box history record (the date_from field) to capture that fact. Get with your financial record-keepers. Display of such metadata is planned for a future release.
    * During an inventory, multiple teams with notebooks or tablets can work in parallel, reducing the amount of time it takes to take an inventory. Each team can select a box, verify the contents, and mark the box as complete before selecting another box.
 
### Consumables Differentiation ###
* Tracks whether items are consumable or not
    * (N) List consumable items, per box, for printing to allow event staff or annual inventory staff to verify quantities remaining and make notes.
        * A stopgap is available on the box report page - type "True" in the search box on each table and click each table's "PDF" button and you have a list. 
        * Or, visit the "Consumables" page, sort the far-right column to display items with "buy soon" or "buy *n* items" rows at the top, then click "print" and limit your print job to the pages showing less than 100% remaining. 

### Image Support ###
* Warehouse detail page shows photo associated with specified warehouse, if set. This could be an image of an individual's or officer's armory, or a photo of the physical warehouse.
    * This defaults to "warehouse.png" to facilitate using a placeholder file.
* Box listing on warehouse detail, box list, and box detail includes photo of specified box, if set. 
    * Defaults to "box.png" to facilitate using a placeholder file.
* Item listing on box detail, item list, and item detail shows item photo, if set. Set photo by saving image file to /static/inv/img and recording the image's filename in the item's record. Recommended image file naming convention: by item ID, item name, or combo. Image name field and image name (including extension, but excluding path) must match exactly. Run python manage.py collectstatic after adding any files to the static/ folder tree. 
    * Each gallery card and detail page displays either a button to use the webcam function to capture a new picture (if it is enabled), or gives a button that displays a modal with information on how to send a photo - including email address set in `PHOTO_CONTACT_EMAIL` if set and what information to provide (object type & ID) to the email recipient. 
* Option to set a thumbnail image to display in gallery page cards, for less file-transfer while still keeping larger images for detail pages. If no thumb is provided, it will fall back to the full-size image.
* There's no placeholder for contact information for the inventory manager, but the global footer would be a good place to put that information. The global footer's markup is in templates/inv/base.html, in between `<footer ...>` and `</footer>`. (in process of adding contact info via settings file, please stand by)
* New Item records do not set a photo filename by default, but this could be accomplished by editing the Items model in inv/models.py, where 'item.png' is the name of your default item image file.
  * Similarly, if you do not want the system to auto-insert `warehouse.png` and `box.png` into new box and warehouse entries, you can edit `inv/models.py` and remove `default='warehouse.png'` and `default='box.png'` respectively from the warehouse and box models.
 ~~~~python
 img/models.py
 ...
 class Items(models.Model):
 ...
 item_img = models.CharField(max_length=50, blank=True, default='item.png')
 ~~~~
 
This app is built on the well-established and well-documented Django framework. If you're not sure how to do something, try checking the Django documentation pages first.

# Changelog #

## Version 2.0 released [Date] (currently RC stage, available in `main` branch)
* Django python package updated to latest version, advancing Django from major version 3 to 4
* opencv-python package updated to latest version, advancing opencv-python from 4.5.3.56 to 4.5.5.64
* templates streamlined; gallery cards all built using the same template
* django-csvimport removed from application due to version change deprecation in Django
* incorporates bug-fixes and some easy enhancements identified by users during an actual use of the system in production (for an event).

## [Version 1.2](https://gitlab.com/cordelya/mobiliaire/-/tags/1.2) released April 10, 2022

* Optional Edit button support added, pulled in from a private fork because it was a darn good idea.
* New field in Items model: item_img_thumb - a place to store a thumbnail filename. Planned use is in gallery cards, where images don't need to be full-size. Gallery templates not yet updated, not included in this release.
* Adjustment to Warehouse detail style/layout so it displays nicely.
* Changed item-count information on Index page plus Warehouse and Box Summary pages
  * Now shows both:
    * Number of Item objects in selection
    * Number of total items in selection, because Item objects can represent multiple identical or similar items

## [Version 1.1](https://gitlab.com/cordelya/mobiliaire/-/tags/1.1) released April 7, 2022
* Webcam support added. Features available in the Raspberry Pi version of this app have been rolled into this repository and support for USB webcams has been added. Administrators will be able to select between no camera, pi camera, and web camera, accordingly. The mobiliaire-raspi repository will receive one final update (making it functionally identical to this repository) and will then no longer recieve updates. Users will be directed to the main repository.

## [Version 1.0.1](https://gitlab.com/cordelya/mobiliaire/-/tags/1.0.1) released October 15, 2020
* Unification of template styling so that warehouse/box and warehouses/boxes/items appear the same. 
* Keyword view styling updated. 
* Items.html keyword filtering styling changed. 
* "Missing photo" placeholder styling changed in several locations.
* index.html total value calculation in views.py corrected
* index.js updated, splitting DataTables initializer into two views, t1 and t2. Currently, t1 is applied to all reports tables, and t-2 is applied to the table on the consumables page - to facilitate showing the dt-SearchBuilder extension for smarter filtering.
* index.js updated to reflect element changes in items.html (from `<li>` to `<button>`)

## [Version 1.0](https://gitlab.com/cordelya/mobiliaire/-/tags/1.0) released October 5, 2020
* Initial Stable Release incorporating all work since project started.
