"""Define Forms for use with the inv app."""

from django import forms
from inv.models import Inventory


class InventoryAddForm(forms.Form):
    """Define a form to insert a single item inventory record.

    Intended to be used in a formset to generate a tabular item view for
    a single box with a comment field and checkbox for each item.
    """

    inv_date = forms.CharField(widget=forms.HiddenInput)
    inv_item = forms.IntegerField(widget=forms.HiddenInput)
    inv_comment = forms.CharField(
        max_length=500,
        widget=forms.TextInput(attrs={"size": 60}),
        required=False,
    )
    addnew = forms.BooleanField(required=False)


class InventoryUpdateForm(forms.ModelForm):
    """Define a form to update existing data in the Inventory table."""

    class Meta:
        model = Inventory
        fields = [
            "inv_id",
            "inv_date",
            "inv_item",
            "inv_comment",
        ]


class UploadImageForm(forms.Form):
    """Define a form to collect an image via file-input."""

    fileName = forms.CharField(widget=forms.HiddenInput)
    imageFile = forms.FileField(
        widget=forms.FileInput(attrs={"accept": "image/*", "capture": "environment"})
    )
