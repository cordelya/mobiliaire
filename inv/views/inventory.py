"""Define inventory views for the inv app."""

from django.shortcuts import render, redirect
from django.conf import settings as conf_settings
from django.forms import formset_factory, modelformset_factory
from django.contrib.auth.decorators import login_required
from inv.forms import InventoryAddForm, InventoryUpdateForm
from inv.models import (
    Boxes,
    Items,
    Items_in_boxes,
    Inventory,
)
from django.db.models import (
    Count,
    F,
    FloatField,
    OuterRef,
    Q,
    Subquery,
    Sum,
)
import datetime


@login_required
def boxInventory(request, boxid):
    """Builds a tabular view of all items in specified box with forms.

    Items are grouped by whether or not an inventory record already exists for
    them on today's date or not. Items with a record offer the opportunity to
    amend or remove the records. Items without a record offer the opportunity
    to add one dated today, with or without a comment.
    """

    # create our reusable Fromset Factories
    InventoryUpdateFormset = modelformset_factory(
        Inventory,
        form=InventoryUpdateForm,
        fields=(
            "inv_id",
            "inv_date",
            "inv_item",
            "inv_comment",
        ),
        can_delete=True,
        extra=0,
    )

    InventoryAddFormset = formset_factory(InventoryAddForm, extra=0)

    # obtain the current date formatted exactly like the values stored in
    # the inv_date annotation
    # a year, month, day was passed via get. Let's set the date to that
    # generally, nobody should be back-dating inventories, however, this
    # functionality might be needed if an inventory was completed on paper,
    # or when initially loading data.
    # Expected url pattern is
    # /inventory/box/<boxid>/?year=year&month=month&day=day
    # if all 3 aren't set or aren't valid, we'll revert back to using the
    # current date
    try:
        y = int(request.GET.get("year"))
        m = int(request.GET.get("month"))
        d = int(request.GET.get("day"))
        t = datetime.date(y, m, d)
    except NameError:
        t = datetime.date.today()
    except TypeError:
        t = datetime.date.today()

    # POST handling goes here
    if request.method == "POST" and request.POST.get("submitUpdate"):
        update_formset = InventoryUpdateFormset(request.POST)

        if update_formset.is_valid():
            update_formset.save()
            return redirect(request.path)
        else:
            for form in update_formset:
                if form.is_valid():
                    form.save()
            return redirect(request.path)

    elif request.method == "POST" and request.POST.get("submitAdd"):
        add_formset = InventoryAddFormset(request.POST)
        if add_formset.is_valid():

            for i in add_formset:
                if i.cleaned_data["addnew"]:
                    date = i.cleaned_data["inv_date"]
                    comment = i.cleaned_data["inv_comment"]
                    item_id = i.cleaned_data["inv_item"]
                    item = Items.objects.filter(item_id=item_id).get()
                    inv = Inventory(inv_date=date, inv_comment=comment, inv_item=item)
                    inv.save()
            return redirect(request.path)
        else:
            print("Error: ", add_formset.errors)
    # we want to display some information about the box itself
    box = Boxes.objects.filter(box_id=boxid).get()

    # this is a subquery for use in constructing the items list
    invs = (
        Inventory.objects.filter(inv_item=OuterRef("item_id"))
        .values("inv_date", "inv_id")
        .order_by("-inv_date")
    )

    # construct the full items list for this box including the
    # last date of inventory if there is one
    items = (
        Items_in_boxes.objects.prefetch_related()
        .filter(date_to__isnull=True)
        .filter(box_id=boxid)
        .annotate(last_inv=Subquery(invs.values("inv_date")[:1]))
        .annotate(last_inv_id=Subquery(invs.values("inv_id")[:1]))
        .annotate(img=F("item_id_id__item_img"))
        .annotate(img_thumb=F("item_id_id__item_img_thumb"))
        .annotate(qty=F("item_id_id__item_qty"))
    )

    # build the querysets that generate items due and items not due
    new_items = items.filter(~Q(last_inv=t) | Q(last_inv__isnull=True))
    old_items = items.filter(Q(last_inv=t))

    if request.method != "POST":
        # we need to build new formsets
        update_keys = list(set(old_items.all().values_list("last_inv_id", flat=True)))
        update_formset = InventoryUpdateFormset(
            queryset=Inventory.objects.filter(inv_id__in=update_keys)
        )
        add_keys = list(set(new_items.all().values_list("item_id", flat=True)))
        add_formset = InventoryAddFormset(
            initial=[{"inv_item": x, "inv_date": t} for x in add_keys]
        )
    # if we're missing the update_formset, it'll cause a problem, so let's
    # check for it and create it if not exists
    try:
        items_updateforms = zip(old_items, update_formset.forms)
    except NameError:
        update_keys = list(set(old_items.all().values_list("last_inv_id", flat=True)))
        update_formset = InventoryUpdateFormset(
            queryset=Inventory.objects.filter(inv_id__in=update_keys)
        )
        items_updateforms = zip(old_items, update_formset.forms)
    try:
        items_addforms = zip(new_items, add_formset.forms)
    except NameError:
        add_keys = list(set(new_items.all().values_list("item_id", flat=True)))
        add_formset = InventoryAddFormset(
            initial=[{"inv_item": x, "inv_date": t} for x in add_keys]
        )
        items_addforms = zip(new_items, add_formset.forms)

    # combine some non-queryset contextual variables:
    today = datetime.date.today()

    context = {"today": today, "t": t}
    edit_buttons = conf_settings.EDIT_BUTTONS
    return render(
        request,
        "inv/boxinventory.html",
        {
            "context": context,
            "box": box,
            "add_formset": add_formset,
            "update_formset": update_formset,
            "updateform": items_updateforms,
            "addform": items_addforms,
            "edit_buttons": edit_buttons,
        },
    )


def inventories(request):
    """Builds page about inventories already completed.

    Returns a render request for inv/inventory.html with variable `invs`
    structured as {'inv_date': datetime.date, 'items': item-count, 'level':
    level, 'coverage': coverage} and variable `context` containing the string
    'inventories' to be contrasted against the view named 'inventory' which
    will pass the same context variable, set to 'inventory'.

    'level' passes Bootstrap table context classes to the template.
    """
    items_total = Items.objects.all().count()
    invs = (
        Inventory.objects.values("inv_date")
        .order_by("inv_date")
        .annotate(items=Count("inv_item"))
    )

    for inv in invs:
        complete = inv["items"]
        total = items_total
        if complete > 0 and total > 0:
            percent = complete / total
        else:
            percent = 0

        # set the color-coding for the progress bar
        match percent:
            case pct if 0 < pct < 0.25:
                inv["level"] = "table-danger"
            case pct if 0.25 < pct < 0.5:
                inv["level"] = "table-warning"
            case pct if 0.5 < pct < 0.75:
                inv["level"] = "table-info"
            case pct if pct >= 0.75:
                inv["level"] = "table-success"

        inv["coverage"] = round((percent * 100), 2)

    edit_buttons = conf_settings.EDIT_BUTTONS
    return render(
        request,
        "inv/inventory.html",
        {"invs": invs, "context": "inventories", "edit_buttons": edit_buttons},
    )


def inventory(request, year, month, day):
    """Builds a page with a report about a single inventory session.

    All items, tabular, organized by warehouse and box.
    """
    date = datetime.date(year, month, day)
    # note: use a date pattern for the url

    # pull the most recent inventory to filter against
    inv = Inventory.objects.filter(inv_date=date).order_by("inv_item")

    boxes = (
        Boxes.objects.values("box_id", "box_name")
        .annotate(
            t_items=Count(
                "bx_id__item_id", distinct=True, filter=Q(bx_id__date_to__isnull=True)
            )
        )
        .annotate(
            c_items=Count(
                "bx_id__item_id__inv_item_id",
                filter=(
                    Q(bx_id__date_to__isnull=True)
                    & Q(bx_id__item_id__inv_item_id__inv_date=date)
                ),
            ),
        )
    )
    for box in boxes:
        complete = int(box["c_items"])
        total = int(box["t_items"])
        incomplete = total - complete
        box["i_items"] = incomplete
        if complete > 0 and total > 0:
            percent = complete / total
        else:
            percent = 0
        box["percent"] = round((percent * 100), 2)

    # edit_buttons = conf_settings.EDIT_BUTTONS
    return render(
        request,
        "inv/inventory.html",
        {"inv": inv, "boxes": boxes, "context": "inventory"},
    )


def inventoryDash(request):
    t = datetime.date.today()

    # subquery: returns list of inventory records for specified item by item_id
    # sorted newest to oldest, for annotating the most recent inventory date on
    # each item in an Items queryset object
    invs = (
        Inventory.objects.filter(inv_item=OuterRef("item_id"))
        .values("inv_date", "inv_id")
        .order_by("-inv_date")
    )

    # grab the list of items with the most recent inventory date annotated
    items = (
        Items.objects.all()
        .prefetch_related()
        .annotate(inv_date=Subquery(invs.values("inv_date")[:1]))
    )
    # set up the totals array and populate it
    totals = []
    total_items = items.all().count()
    total_items_complete = items.all().filter(inv_date=t).count()
    percent_complete = 0
    if total_items > 0 and total_items_complete > 0:
        percent_complete = (total_items_complete / total_items) * 100
    totals = {
        "total_items": total_items,
        "total_items_complete": total_items_complete,
        "percent": percent_complete,
    }

    # grab the list of boxes, annotated with total items per box and completed
    # items per box
    boxes = (
        Boxes.objects.all()
        .annotate(
            t_items=Count("bx_id__item_id", filter=Q(bx_id__date_to__isnull=True))
        )
        .annotate(
            c_items=Count(
                "bx_id__item_id__inv_item_id",
                filter=(
                    Q(bx_id__date_to__isnull=True)
                    & Q(bx_id__item_id__inv_item_id__inv_date=t)
                ),
            )
        )
    )
    for box in boxes:
        complete = box.c_items
        total = box.t_items
        if complete > 0 and total > 0:
            percent = complete / total
        else:
            percent = 0
        box.level = "bg-danger"
        if percent >= 0.25:
            box.level = "bg-warning"
        if percent >= 0.5:
            box.level = "bg-info"
        if percent >= 0.75:
            box.level = "bg-success"
        box.percent = percent * 100

    edit_buttons = conf_settings.EDIT_BUTTONS
    return render(
        request,
        "inv/inventorydash.html",
        {
            "items": items,  # this may not be needed
            "totals": totals,
            "boxes": boxes,
            "edit_buttons": edit_buttons,
        },
    )


def InventoryLookout(request):
    """Build a page showing all items not inventoried within past 365 days.

    Organization: by box, with each box's due items listed in a table that
    includes the item's most recent inventory date if there is one.
    """
    today = datetime.date.today()
    ly = today - datetime.timedelta(days=365)

    # a subquery to fetch an item's Inventory dates, ordered most-to-least
    # recent
    invs = (
        Inventory.objects.filter(inv_item=OuterRef("item_id"))
        .values("inv_date", "inv_id")
        .order_by("-inv_date")
    )

    # list of items annotated with most recent inventory date,
    # filtered to only include due items,
    # annotated with box_id
    items = (
        Items.objects.prefetch_related()
        .filter(itm_id__date_to__isnull=True)
        .annotate(inv_date=Subquery(invs.values("inv_date")[:1]))
        .filter(Q(inv_date__lte=ly) | Q(inv_date__isnull=True))
        .annotate(box_id=F("itm_id__box_id"))
    )

    boxes = Boxes.objects.values("box_id", "box_name")
    edit_buttons = conf_settings.EDIT_BUTTONS
    return render(
        request,
        "inv/inventory-lookout.html",
        {
            "boxes": boxes,
            "items": items,
            "edit_buttons": edit_buttons,
        },
    )
