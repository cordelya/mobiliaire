"""Define views for the inv app."""

from django.shortcuts import render
from django.conf import settings as conf_settings
from inv.models import (
    Warehouse,
    Boxes,
    Items,
    Keywords,
)
from django.db.models import (
    Count,
    F,
    FloatField,
    Sum,
)
from django.db.models.functions import Lower


def isMobile(user_agent):
    """Examine user-agent string for presence of mobile browser keywords."""

    keywords = ["Mobile", "iPhone", "Android", "Opera Mini"]
    if any(keyword in user_agent for keyword in keywords):
        return True
    else:
        return False


def index(request):
    """Collate information about warehouses, boxes, & items.

    for building the index page
    information queried and passed to index page template:
    total warehouse & box counts.
    total item entries plus total items represented
    total value of all items represented
    bool template will use to determine whether to the
    'manage data' link in the header navbar
    """
    edit_buttons = conf_settings.EDIT_BUTTONS
    item_count = Items.objects.all().count()
    i_total_a = Items.objects.all().filter(item_qty__isnull=True).count()
    i_total_b = Items.objects.all().filter(item_qty__exact=0).count()
    i_total_c = Items.objects.all().filter(item_qty__gt=0).aggregate(Sum("item_qty"))
    item_total = (i_total_a or 0) + (i_total_b or 0) + (i_total_c["item_qty__sum"] or 0)
    box_count = Boxes.objects.all().annotate(Count("box_id"))
    warehouse_count = Warehouse.objects.all().annotate(Count("warehouse_id"))
    val = (
        Items.objects.all()
        .annotate(ival=Sum(F("item_value") * F("item_qty"), output_field=FloatField()))
        .aggregate(val=Sum("ival"))
    )
    return render(
        request,
        "inv/index.html",
        {
            "item_count": item_count,
            "item_total": item_total,
            "box_count": box_count,
            "warehouse_count": warehouse_count,
            "val": val,
            "edit_buttons": edit_buttons,
        },
    )


def dash(request):
    """Builds a dashboard.

    not yet implemented
    """
    edit_buttons = conf_settings.EDIT_BUTTONS
    return render(request, "inv/dash.html", {"edit_buttons": edit_buttons})


def search(request):
    """Search database for matches.

    given a simple search term, this returns results of queries against
    the database for exact string matches
    additionally passes variables relating to options: camera, edit_buttons,
    photo_email
    """
    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False
    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    if request.method == "GET":
        search = request.GET.get("search")
        if search is None:
            return render(request, "inv/search.html", {"edit_buttons": edit_buttons})
        else:
            result = (
                Items.objects.filter(item_name__icontains=search)
                .filter(itm_id__date_to__isnull=True)
                .prefetch_related()
                .annotate(box=F("itm_id__box_id__box_name"))
                .annotate(boxid=F("itm_id__box_id"))
                .annotate(wh=F("itm_id__box_id__warehouse__warehouse_name"))
                .annotate(whid=F("itm_id__box_id__warehouse"))
                .annotate(
                    totval=Sum(
                        F("item_value") * F("item_qty"), output_field=FloatField()
                    )
                )
                .annotate(sort_name=Lower("item_name"))
                .order_by("sort_name")
            )
            kw_result = Keywords.objects.filter(keyword__icontains=search)
            box_result = Boxes.objects.filter(box_name__icontains=search)
            wh_result = Warehouse.objects.filter(warehouse_name__icontains=search)
            kw = Keywords.objects.only("keyword").order_by("keyword")
            return render(
                request,
                "inv/search.html",
                {
                    "result": result,
                    "kw_result": kw_result,
                    "kw": kw,
                    "box_result": box_result,
                    "wh_result": wh_result,
                    "camera": camera,
                    "edit_buttons": edit_buttons,
                    "photo_email": photo_email,
                    "mobile": mobile,
                },
            )
    else:
        return render(request, "inv/search.html", {"edit_buttons": edit_buttons})
