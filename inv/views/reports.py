"""Define views for the inv app."""

from django.shortcuts import render
from django.conf import settings as conf_settings
from inv.models import (
    Warehouse,
    Boxes,
    Items,
    Keywords_in_items,
)
from django.db.models import (
    Count,
    ExpressionWrapper,
    F,
    FloatField,
    Sum,
)
from django.db.models.functions import Lower


def reports(request):
    """Builds reports landing page."""

    edit_buttons = conf_settings.EDIT_BUTTONS
    return render(request, "inv/reports.html", {"edit_buttons": edit_buttons})


def report_itm(request, item_sort=None):
    """Generate a full list of all items in the database.

    includes information about each item's associated box and warehouse,
    each item's total value (items can be multiples), and associated keywords
    """
    edit_buttons = conf_settings.EDIT_BUTTONS
    items = (
        Items.objects.all()
        .prefetch_related()
        .annotate(box=F("itm_id__box_id__box_name"))
        .annotate(wh=F("itm_id__box_id__warehouse__warehouse_name"))
        .annotate(
            totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
        )
        .annotate(kw=F("item_id"))
    )
    kw = Keywords_in_items.objects.all()
    for k in kw:
        k.kw = k.keyword_id.replace(": ", "-")
    return render(
        request,
        "inv/rpt_items.html",
        {"items": items, "kw": kw, "edit_buttons": edit_buttons},
    )


def report_box(request, box=None):
    """Query for a single box if specified, else query for all boxes.

    includes information about related warehouse, total number of items,
    total value of items, and a full list of item names. Passes to the
    box report template.
    """
    edit_buttons = conf_settings.EDIT_BUTTONS
    if box:
        boxes = Boxes.objects.filter(box_id=box)
    else:
        boxes = Boxes.objects.all()
    items = (
        Items.objects.all()
        .prefetch_related()
        .annotate(boxid=F("itm_id__box_id"))
        .annotate(
            totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
        )
        .annotate(sort_name=Lower("item_name"))
        .order_by("sort_name")
    )
    return render(
        request,
        "inv/rpt_boxes.html",
        {"items": items, "boxes": boxes, "edit_buttons": edit_buttons},
    )


def report_wh(request, whid=None):
    """Query for a specific warehouse if specified, else query all warehouses.

    returns all boxes contained within warehouse and all items contained
    within all attached boxes.
    """
    edit_buttons = conf_settings.EDIT_BUTTONS
    if whid:
        wh = Warehouse.objects.filter(warehouse_id=whid)
    else:
        wh = Warehouse.objects.all()
    box = (
        Boxes.objects.all()
        .prefetch_related()
        .annotate(whid=F("warehouse__warehouse_id"))
    )
    items = items = (
        Items.objects.all()
        .prefetch_related()
        .annotate(boxid=F("itm_id__box_id"))
        .annotate(
            totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
        )
        .annotate(sort_name=Lower("item_name"))
        .order_by("sort_name")
    )

    return render(
        request,
        "inv/rpt_wh.html",
        {"items": items, "wh": wh, "box": box, "edit_buttons": edit_buttons},
    )


def report_full(request):
    """Query for all warehouses, boxes, and items for a full report.

    plus a list of boxes sorted both A-Z and numerically by box_id
    """
    # full all-in-one inventory report for exporting via dataTables
    edit_buttons = conf_settings.EDIT_BUTTONS
    wh = (
        Warehouse.objects.all()
        .prefetch_related()
        .annotate(box_count=Count("bx_wh__box_id"))
    )
    box = (
        Boxes.objects.all()
        .prefetch_related()
        .annotate(whid=F("warehouse__warehouse_id"))
    )
    box_alpha = (
        Boxes.objects.all()
        .prefetch_related()
        .annotate(whid=F("warehouse__warehouse_id"))
        .annotate(sort_name=Lower("box_name"))
        .order_by("sort_name")
    )
    items = items = (
        Items.objects.all()
        .prefetch_related()
        .annotate(boxid=F("itm_id__box_id"))
        .annotate(
            totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
        )
        .annotate(sort_name=Lower("item_name"))
        .order_by("sort_name")
    )

    return render(
        request,
        "inv/rpt_full.html",
        {
            "items": items,
            "wh": wh,
            "box": box,
            "box_alpha": box_alpha,
            "edit_buttons": edit_buttons,
        },
    )


def consumables(request):
    """Build a list of all items marked as 'consumable'.

    includes percentage remaining, calculated at query time
    """
    edit_buttons = conf_settings.EDIT_BUTTONS
    consumables = Items.objects.filter(item_consumable=True).annotate(
        restock=ExpressionWrapper(
            F("item_qty") * ((100.0 - F("item_remaining")) / 100.0),
            output_field=FloatField(),
        )
    )
    return render(
        request,
        "inv/consumables.html",
        {"consumables": consumables, "edit_buttons": edit_buttons},
    )
