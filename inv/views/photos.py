"""Define photo-capturing views for the inv app."""

from django.shortcuts import render, redirect
from django.conf import settings as conf_settings
from inv.forms import UploadImageForm
from time import sleep
import datetime


def cameraCapture(request, fileName=None, fname=None, method=None):
    """given a fileName and method, capture an image and save as fileName."""
    now = datetime.datetime.now()
    now = now.strftime("%Y-%m-%d_%H-%M-%S")
    proj_path = str(conf_settings.BASE_DIR)
    if fname:
        return render(
            request,
            "inv/photo.html",
            {
                "fileName": fileName,
                "fname": fname,
            },
        )
    elif fileName:
        if method == "upload":
            # If the request method is POST, process the form data
            if request.method == "POST":
                form = UploadImageForm(request.POST, request.FILES)
                if form.is_valid():
                    try:
                        with request.FILES["imageFile"] as img:
                            img_name = img.name
                            ext = img_name.split(".")[1].lower()
                            fname = fileName + "-" + now + "." + ext
                            fname = str(fname)
                            base = str(conf_settings.MOBILE_UPLOADS_PATH)
                            save_path = "{}/{}".format(base, fname)
                            with open(save_path, "wb+") as dest:
                                for chunk in img.chunks():
                                    dest.write(chunk)
                        return redirect("/success/{}/{}/".format(fileName, fname))
                    except OSError as e:
                        return render(request, "inv/upload.html", {"error": e})

            # Otherwise, render the form
            else:
                form = UploadImageForm(initial={"fileName": fileName})
                return render(
                    request, "inv/upload.html", {"form": form, "fileName": fileName}
                )
        elif method == "picamera":
            # at present only works while running the development server due to
            # peripherals access
            from picamera import PiCamera

            fname = fileName + "_" + now + ".jpg"
            fname = str(fname)
            camera = PiCamera()
            # if pictures aren't coming out upright, adjust this next variable.
            # Options: 0, 90, 180, 270.
            camera.rotation = 0
            camera.start_preview()
            sleep(5)
            savename = str("%s/static/inv/new/%s" % (proj_path, fname))
            camera.capture(savename)
            camera.stop_preview()
            camera.close()
            return redirect("/success/%s/%s/" % (fileName, fname))
        elif method == "webcam":
            # at present, this only works while running the development server
            # due to peripherals access
            import cv2

            fname = fileName + "_" + now + ".jpg"
            fname = str(fname)
            # this takes the camera index as an argument. If you have more than
            # one camera, you may need to adjust the index from (0) to some
            # other number.
            camera = cv2.VideoCapture(0)
            ret, frame = camera.read()
            savename = str("%s/static/inv/new/%s" % (proj_path, fname))
            cv2.imwrite(savename, frame)
            camera.release()
            cv2.destroyAllWindows()
            return redirect("/success/%s/%s/" % (fileName, fname))
