"""Define gallery views for the inv app."""

from django.shortcuts import render
from django.conf import settings as conf_settings
from inv.models import (
    Warehouse,
    Boxes,
    Items,
    Items_in_boxes,
    Keywords,
    Keywords_in_items,
)
from django.db.models import (
    Count,
    F,
    FloatField,
    Q,
    Sum,
)
from django.db.models.functions import Lower
from .views import isMobile


def items(request):
    """Build item gallery page.

    Includes related warehouse and box for each item, quantities,
    values, and keywords
    """
    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False

    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    items = (
        Items.objects.all()
        .prefetch_related()
        .annotate(box=F("itm_id__box_id__box_name"))
        .annotate(boxid=F("itm_id__box_id"))
        .annotate(wh=F("itm_id__box_id__warehouse__warehouse_name"))
        .annotate(whid=F("itm_id__box_id__warehouse"))
        .annotate(
            totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
        )
        .annotate(sort_name=Lower("item_name"))
        .order_by("sort_name")
    )
    kw = Keywords.objects.only("keyword").order_by("keyword")
    return render(
        request,
        "inv/items.html",
        {
            "items": items,
            "kw": kw,
            "camera": camera,
            "edit_buttons": edit_buttons,
            "photo_email": photo_email,
            "mobile": mobile,
        },
    )


def item(request, itemid):
    """Build item detail page."""

    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False

    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    item = (
        Items.objects.filter(item_id=itemid)
        .annotate(
            totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
        )
        .get(item_id=itemid)
    )
    box = (
        Items_in_boxes.objects.filter(item_id=itemid)
        .filter(date_to__isnull=True)
        .annotate(name=F("box_id__box_name"))
        .annotate(bx_id=F("box_id"))
        .get(item_id=itemid)
    )
    wh = (
        Boxes.objects.filter(box_id=box.bx_id)
        .annotate(name=F("warehouse__warehouse_name"))
        .get(box_id=box.bx_id)
    )
    kw = Keywords_in_items.objects.filter(item_id=itemid)
    return render(
        request,
        "inv/item.html",
        {
            "item": item,
            "box": box,
            "wh": wh,
            "kw": kw,
            "camera": camera,
            "edit_buttons": edit_buttons,
            "photo_email": photo_email,
            "mobile": mobile,
        },
    )


def boxes(request):
    """Build boxes gallery page.

    includes information about related warehouse and number of items within
    """
    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False

    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    boxes = Boxes.objects.all().annotate(
        num_items=Count("bx_id__item_id", filter=Q(bx_id__date_to__isnull=True))
    )
    return render(
        request,
        "inv/boxes.html",
        {
            "boxes": boxes,
            "camera": camera,
            "edit_buttons": edit_buttons,
            "photo_email": photo_email,
            "mobile": mobile,
        },
    )


def box(request, boxid):
    """Build box detail page with gallery of box's items.

    includes info about related warehouse and total value of items in box.
    """
    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False

    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    box = Boxes.objects.prefetch_related("warehouse").get(box_id=boxid)
    items = (
        Items_in_boxes.objects.prefetch_related()
        .filter(date_to__isnull=True)
        .filter(box_id=boxid)
        .annotate(item_img=F("item_id__item_img"))
        .annotate(
            totval=Sum(
                F("item_id__item_value") * F("item_id__item_qty"),
                output_field=FloatField(),
            )
        )
    )
    items.item_totals = (
        Items_in_boxes.objects.filter(date_to__isnull=True)
        .filter(box_id=boxid)
        .aggregate(icount=Count("item_id"), val=Sum("item_id__item_value"))
    )
    i_total_a = (
        Items_in_boxes.objects.prefetch_related()
        .filter(date_to__isnull=True)
        .filter(box_id=boxid)
        .filter(item_id__item_qty__isnull=True)
        .count()
    )
    i_total_b = (
        Items_in_boxes.objects.prefetch_related()
        .filter(date_to__isnull=True)
        .filter(box_id=boxid)
        .filter(item_id__item_qty__exact=0)
        .count()
    )
    i_total_c = (
        Items_in_boxes.objects.prefetch_related()
        .filter(date_to__isnull=True)
        .filter(box_id=boxid)
        .filter(item_id__item_qty__gt=0)
        .aggregate(Sum("item_id__item_qty"))
    )
    items_sum = (
        (i_total_a or 0) + (i_total_b or 0) + (i_total_c["item_id__item_qty__sum"] or 0)
    )
    return render(
        request,
        "inv/box.html",
        {
            "box": box,
            "items": items,
            "items_sum": items_sum,
            "camera": camera,
            "edit_buttons": edit_buttons,
            "photo_email": photo_email,
            "mobile": mobile,
        },
    )


def warehouses(request):
    """Build warehouses gallery page."""

    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False

    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    wh = Warehouse.objects.all().annotate(num_boxes=Count("bx_wh"))
    return render(
        request,
        "inv/warehouses.html",
        {
            "wh": wh,
            "camera": camera,
            "edit_buttons": edit_buttons,
            "photo_email": photo_email,
            "mobile": mobile,
        },
    )


def warehouse(request, whid):
    """Build warehouse detail page with gallery of warehouse's boxes.

    includes a list of all boxes contained within, item totals and total
    values
    """
    if conf_settings.MOBILE_UPLOADS:
        mobile = isMobile(request.META["HTTP_USER_AGENT"])
    else:
        mobile = False

    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    wh = Warehouse.objects.get(warehouse_id=whid)
    boxes = (
        Boxes.objects.filter(warehouse_id=whid)
        .annotate(num_items=Count("bx_id", filter=Q(bx_id__date_to__isnull=True)))
        .annotate(
            val=Sum("bx_id__item_id__item_value", filter=Q(bx_id__date_to__isnull=True))
        )
    )
    total_boxes = Warehouse.objects.filter(warehouse_id=whid).aggregate(
        boxes=Count("bx_wh")
    )
    total_items = (
        Warehouse.objects.filter(warehouse_id=whid)
        .filter(bx_wh__bx_id__date_to__isnull=True)
        .aggregate(items=Count("bx_wh__bx_id__item_id"))
    )
    total_itemval = (
        Warehouse.objects.filter(warehouse_id=whid)
        .filter(bx_wh__bx_id__date_to__isnull=True)
        .aggregate(
            val=Sum(
                (
                    F("bx_wh__bx_id__item_id__item_value")
                    * F("bx_wh__bx_id__item_id__item_qty")
                ),
                output_field=FloatField(),
            )
        )
    )
    items_sum = (
        Warehouse.objects.filter(warehouse_id=whid)
        .filter(bx_wh__bx_id__date_to__isnull=True)
        .values("bx_wh__bx_id__item_id__item_qty")
        .aggregate(total=Sum("bx_wh__bx_id__item_id__item_qty"))
    )
    return render(
        request,
        "inv/warehouse.html",
        {
            "wh": wh,
            "boxes": boxes,
            "camera": camera,
            "edit_buttons": edit_buttons,
            "total_boxes": total_boxes,
            "total_items": total_items,
            "items_sum": items_sum,
            "total_itemval": total_itemval,
            "photo_email": photo_email,
            "mobile": mobile,
        },
    )


def keywords(request, kw_slug=None):
    """Build a gallery page showing a list of items associated with keyword.

    or a full list of keywords if kw_slug is not set.
    """
    camera = conf_settings.CAMERA
    edit_buttons = conf_settings.EDIT_BUTTONS
    photo_email = conf_settings.PHOTO_CONTACT_EMAIL
    if kw_slug:
        keyword = Keywords.objects.get(keyword_slug=kw_slug)
        kw = Keywords_in_items.objects.prefetch_related().filter(
            keyword__keyword_slug=kw_slug
        )
        items = (
            Items.objects.prefetch_related()
            .filter(kw_itm__keyword__keyword_slug=kw_slug)
            .annotate(box=F("itm_id__box_id__box_name"))
            .annotate(boxid=F("itm_id__box_id"))
            .annotate(wh=F("itm_id__box_id__warehouse__warehouse_name"))
            .annotate(whid=F("itm_id__box_id__warehouse"))
            .annotate(
                totval=Sum(F("item_value") * F("item_qty"), output_field=FloatField())
            )
            .annotate(sort_name=Lower("item_name"))
            .order_by("sort_name")
        )
        return render(
            request,
            "inv/keywords.html",
            {
                "keyword": keyword,
                "kw": kw,
                "items": items,
                "camera": camera,
                "edit_buttons": edit_buttons,
                "photo_email": photo_email,
            },
        )
    else:
        kw = (
            Keywords.objects.all()
            .prefetch_related()
            .annotate(tot=Count("kw_kw__item_id"))
        )
        return render(
            request,
            "inv/keywords.html",
            {
                "kw": kw,
                "camera": camera,
                "edit_buttons": edit_buttons,
                "photo_email": photo_email,
            },
        )
