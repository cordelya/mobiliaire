"""Define database schema."""

from django.db import models
import datetime
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.template.defaultfilters import slugify


class Warehouse(models.Model):
    """Define the Warehouse table."""

    warehouse_id = models.AutoField(primary_key=True)
    warehouse_name = models.CharField(max_length=100, default="warehouse")
    warehouse_loc = models.CharField(max_length=200, blank=True)
    warehouse_details = models.CharField(max_length=300, blank=True)
    warehouse_img = models.CharField(max_length=50, blank=True, default="warehouse.png")
    warehouse_img_thumb = models.CharField(max_length=50, blank=True)

    class Meta:
        """Specify plural form of 'warehouse'."""

        verbose_name_plural = "Warehouses"

    def __str__(self):
        """Use the 'warehouse_name' field to refer to self."""
        return "%s" % (self.warehouse_name)


class Staff(models.Model):
    """Define the Staff table."""

    staff_id = models.AutoField(primary_key=True)
    staff_name = models.CharField(max_length=100, default="staff name")
    staff_title = models.CharField(max_length=50, blank=True)
    staff_contact = models.CharField(max_length=100, blank=True)

    class Meta:
        """Specify plural form of 'staff'."""

        verbose_name_plural = "Staff"

    def __str__(self):
        """Use 'staff_name | staff_title' to refer to self."""
        return "%s | %s" % (self.staff_name, self.staff_title)


class Boxes(models.Model):
    """Define the Boxes table."""

    box_id = models.AutoField(primary_key=True)
    box_name = models.CharField(max_length=20, default="box")
    warehouse = models.ForeignKey(
        Warehouse, on_delete=models.CASCADE, related_name="bx_wh", default=1
    )
    box_location = models.CharField(max_length=300, blank=True)
    box_description = models.CharField(max_length=300, blank=True)
    other_box_details = models.CharField(max_length=300, blank=True)
    box_img = models.CharField(max_length=50, blank=True, default="box.png")
    box_img_thumb = models.CharField(max_length=50, blank=True)

    class Meta:
        """Specify plural form of 'box'."""

        verbose_name_plural = "Boxes"

    def __str__(self):
        """Use 'box_id) box_name' to refer to self."""
        return "%s) %s" % (self.box_id, self.box_name)


class Items(models.Model):
    """Define the Items table."""

    item_id = models.AutoField(primary_key=True)
    item_name = models.CharField(max_length=50, default="name")
    item_owner_staff_id = models.ForeignKey(
        Staff, on_delete=models.CASCADE, related_name="owner", default=1
    )
    item_desc = models.CharField(max_length=500, blank=True)
    item_img = models.CharField(max_length=50, blank=True)
    item_img_thumb = models.CharField(max_length=50, blank=True)
    item_qty = models.IntegerField(default="1")
    item_value = models.DecimalField(max_digits=8, decimal_places=2, blank=True)
    item_consumable = models.BooleanField(default=False)
    item_remaining = models.IntegerField(
        verbose_name="Percent Remaining",
        default=100,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
    )

    class Meta:
        """Specify plurl of item."""

        verbose_name_plural = "Items"

    def __str__(self):
        """Use 'item_id: item_name' to refer to self."""
        return "%s: %s" % (self.item_id, self.item_name)


class Items_in_boxes(models.Model):
    """Define the Items_in_boxes table.

    This table joins items from the Items table with a box from the Boxes
    table. It retains an item's history when moved to a new box."""

    txn_id = models.AutoField(primary_key=True)
    item_id = models.ForeignKey(Items, on_delete=models.CASCADE, related_name="itm_id")
    box_id = models.ForeignKey(Boxes, on_delete=models.CASCADE, related_name="bx_id")
    date_from = models.DateField(default=timezone.now)
    date_to = models.DateField(blank=True, null=True)
    moved_by_staff_id = models.ForeignKey(Staff, on_delete=models.CASCADE, default=1)
    reason = models.CharField(max_length=100, default="add to database")

    class Meta:
        """Specify the plural of 'item in box'."""

        verbose_name_plural = "Items in Boxes"


class Keywords(models.Model):
    """Define the Keywords table."""

    keyword = models.CharField(primary_key=True, max_length=50)
    keyword_desc = models.CharField(max_length=100, blank=True)
    keyword_slug = models.CharField(max_length=50, blank=True)

    class Meta:
        """Specify the plural of 'keyword'."""

        verbose_name_plural = "Keywords"

    def save(self, *args, **kwargs):
        """Slugify the keyword name and store in the keyword_slug field.

        Do this automatically any time a keyword entry is saved."""
        self.keyword_slug = slugify(self.keyword)
        super(Keywords, self).save(*args, **kwargs)

    def __str__(self):
        """Use 'keyword' to refer to self."""
        return "%s" % (self.keyword)


class Keywords_in_items(models.Model):
    """Define the Keywords_in_items table.

    This table joins Keywords in the Keywords table with Items in the Items
    table."""

    item_id = models.ForeignKey(Items, on_delete=models.CASCADE, related_name="kw_itm")
    keyword = models.ForeignKey(
        Keywords, on_delete=models.CASCADE, related_name="kw_kw"
    )

    class Meta:
        """Specify the plural of 'keyword in item'."""

        verbose_name_plural = "Keywords in Items"


class Inventory(models.Model):
    """Define the Inventory table.

    Each item is assigned a row associated with a single date, set
    automatically as the current date."""

    inv_id = models.AutoField(primary_key=True)
    inv_date = models.DateField(default=datetime.date.today)
    inv_item = models.ForeignKey(
        Items, on_delete=models.CASCADE, blank=True, related_name="inv_item_id"
    )
    inv_comment = models.CharField(
        max_length=500, default="an inventory was conducted", blank=True
    )

    class Meta:
        """Specify plural of 'inventory'."""

        verbose_name_plural = "Inventories"

    def __str__(self):
        """Use '<date>: <item> | comment: <comment>' to refer to self."""
        return "%s: Item %s | comment: %s" % (
            self.inv_date,
            self.inv_item,
            self.inv_comment,
        )
