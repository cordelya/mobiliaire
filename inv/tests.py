"""Define Unit tests here."""

from django.test import TestCase


class BasicTestTest(TestCase):
    def setUp(self):
        # Setup tasks run before every test method in this class
        pass

    def tearDown(self):
        # Clean up tasks run after every test method.
        pass

    def test_something_that_will_pass(self):
        self.assertFalse(False)

    def test_something_that_will_fail(self):
        # self.assertTrue(false)
        # We commented the above line because we don't want it to fail right
        # now. It's just an example.
        pass


# Create your tests here.
