"""Specify settings for the admin side."""

from django.contrib import admin
from .models import (
    Warehouse,
    Staff,
    Boxes,
    Items,
    Items_in_boxes,
    Keywords,
    Keywords_in_items,
    Inventory,
)


class WarehouseInLine(admin.TabularInline):
    """Specify style and included models for the Warehouse inline."""

    model = Boxes
    extra = 0


class WarehouseAdmin(admin.ModelAdmin):
    """Specify config for the Warehouse Admin view.

    Depends on WarehouseInLine"""

    list_display = ("warehouse_name", "warehouse_loc")
    inlines = [
        WarehouseInLine,
    ]
    extra = 0


class StaffAdmin(admin.ModelAdmin):
    """Specify config for the Staff Admin view."""

    list_display = ("staff_name", "staff_title")
    extra = 0


class BoxesAdmin(admin.ModelAdmin):
    """Specify config for the Boxes Admin view."""

    list_display = ("box_id", "warehouse", "box_description")
    extra = 0


class ItemsInLine(admin.TabularInline):
    """Specify style and included models for the Items inline."""

    model = Items_in_boxes
    extra = 0


class Keywords_Items(admin.TabularInline):
    """Specify config for the Keywords in Items Inline."""

    model = Keywords_in_items
    extra = 0


class ItemsAdmin(admin.ModelAdmin):
    """Specify config for the Items Admin view."""

    list_display = ("item_name", "item_desc", "item_value")
    inlines = [ItemsInLine, Keywords_Items]
    extra = 0


class Items_in_boxesAdmin(admin.ModelAdmin):
    """Specify config for the Items in Boxes Admin view."""

    list_display = (
        "box_id",
        "item_id",
        "date_from",
        "date_to",
        "moved_by_staff_id",
        "reason",
    )
    extra = 0


class Keywords_itemsAdmin(admin.ModelAdmin):
    """Specify config for the Keywords in Items Admin View.

    Depends on Keywords_Items."""

    list_display = (
        "item_id",
        "keyword",
    )
    extra = 0


class KeywordsAdmin(admin.ModelAdmin):
    """Specify config for the Keywords Admin View."""

    model = Keywords
    list_display = ("keyword", "keyword_desc", "keyword_slug")
    extra = 0


class InventoryAdmin(admin.ModelAdmin):
    """Specify config for Inventory Admin View."""

    model = Inventory
    list_display = (
        "inv_item",
        "inv_date",
        "inv_comment",
        "inv_item__item_id__item_description",
    )


admin.site.register(Warehouse, WarehouseAdmin)
admin.site.register(Staff, StaffAdmin)
admin.site.register(Boxes, BoxesAdmin)
admin.site.register(Items, ItemsAdmin)
admin.site.register(Items_in_boxes, Items_in_boxesAdmin)
admin.site.register(Keywords, KeywordsAdmin)
admin.site.register(Keywords_in_items, Keywords_itemsAdmin)
admin.site.register(Inventory)


# Register your models here.
