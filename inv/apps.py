"""Configure apps here."""

from django.apps import AppConfig


class InvConfig(AppConfig):
    """Tell Django to look for an app named 'inv'."""

    name = "inv"
