# Merge Request

Before this merge request is accepted, the following items should be verified:

Minimally:
- [ ] Changed files have been passed through Black
- [ ] Changed files have been checked by flake8 and items corrected
  -  _Note: migration files may be excluded / ignored_
- [ ] Changed files have been grepped for "FIXME":
  - Before committing, you can: `git diff -G '[Ff][Ii][Xx][Mm][Ee]'`
  - After committing, you can: `grep -rl '[Ff][Ii][Xx][Mm][Ee]' --exclude-dir="venv"`
- [ ] Passes CI pipeline


Maximally:
 - Install [Pre-commit](https://pre-commit.com/) before you begin any work
   -  `pip install pre-commit`
   - There's a `.pre-commit-config.yaml` file already in the repo for it to use.
   - It will check everything you commit, before you commit it, with the notable exception of grepping for FIXME
